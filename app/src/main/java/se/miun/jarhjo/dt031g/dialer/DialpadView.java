package se.miun.jarhjo.dt031g.dialer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.TextViewCompat;

import java.util.Locale;

public class DialpadView extends ConstraintLayout {
    private String number = "";
    private TextView tvText;
    private ImageButton ibtnDel, ibtnCall;
    private DialActivity dialActivity;

    public DialpadView(Context context) {
        super(context);
        init(context, null);
    }

    public DialpadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DialpadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initilizes the view
     * @param context - The context of the view
     * @param attrs - an attribute set
     */
    @SuppressLint("ClickableViewAccessibility")
    private void init(Context context, AttributeSet attrs) {
        // STYLE THE LAYOUT
        //this.setBackgroundColor(Color.LTGRAY);

        // SET INFLATED LAYOUT
        inflate(context, R.layout.dialpad_view, this);
        tvText = (TextView) findViewById(R.id.tvText);
        ibtnDel = (ImageButton) findViewById(R.id.ibtnDel);
        ibtnCall = (ImageButton) findViewById(R.id.ibtnCall);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvText.addTextChangedListener(new PhoneNumberFormattingTextWatcher(Locale.getDefault().getCountry()));
        }else {
            tvText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }

        ibtnDel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelClick(v);
            }
        });

        ibtnDel.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onDelLongClick(v);
                return true;
            }
        });

        ibtnCall.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onCallClick(v);
            }
        });
    }

    public void addChar(char c) {
        setText(number+=c);


    }

    public String getText() {
        return number;
    }

    public void setText(String str) {
        number = str;
        tvText.setText(number);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(tvText, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
    }

    public void onCallClick(View view) {
        Log.d("DialpadView.onCallClick","Begin click");
        if (getDialActivity()!=null)
            getDialActivity().call(getText());

    }

    public void onDelClick(View view) {
        Log.d("DialpadView.onDelClick","Begin click");
        String str = getText();
        int l = Math.max(0,str.length()-1);
        setText(str.substring(0,l));
    }

    public void onDelLongClick(View view) {
        setText("");
    }

    public DialActivity getDialActivity() {
        return dialActivity;
    }

    public void setDialActivity(DialActivity dialActivity) {
        this.dialActivity = dialActivity;
    }

}




