package se.miun.jarhjo.dt031g.dialer;



import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.util.List;



public class PhoneDatabase extends AsyncTask<String, Void, List<Call>> {
    Context context;
    CallListReader callListReader = null;
    CallList callList = null;


    PhoneDatabase(Context context) {
        this.context =  context;
    }

    @Override
    protected List<Call> doInBackground(String... strings) {
        String operation = strings[0];
        CallListDatabase database;
        List<Call> result = null;
        switch (operation) {
            case "add":
                String phoneNumber  = strings[1];
                String time         = strings[2];
                String latitude     = strings[3];
                String longitude    = strings[4];
                database = CallListDatabase.getInstance(context);
                database.addNumber(phoneNumber, time, latitude, longitude);
                break;
            case "getAll":
                database = CallListDatabase.getInstance(context);
                result=database.getAll();
                callList.calls=result;
                break;
            case "delete":
                Toast.makeText(context, "SQLite DELETE OPERATION", Toast.LENGTH_SHORT).show();
                break;
            default:
                Log.d("PhoneDatabase", "Default action: [" + operation + "]");
        }

        return result;
    }

    @Override
    protected void onPostExecute(List<Call> calls) {
        super.onPostExecute(calls);
        if (callListReader!=null)
            callListReader.readCalls(calls);
        Log.d("PhoneDatabase.onPost"," "+((calls != null) ? calls.size(): -1));
        this.context = null;
    }
}

