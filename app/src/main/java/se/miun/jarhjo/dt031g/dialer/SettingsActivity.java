package se.miun.jarhjo.dt031g.dialer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.FileUtils;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreference;
import androidx.preference.SwitchPreferenceCompat;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    public final static String voiceDownloadSavePath = "/sdcard/dialpad/sounds/";

    public final static String CALL_LIST_KEY        = "CALL_LIST_KEY";
    public final static String STORE_NUMBERS        = "STORE_NUMBERS";
    public final static String VOICE_STORAGE_PATH   = "VOICE_STORAGE_PATH";
    public final static String VOICE_DELETE         = "VOICE_DELETE";
    public final static String VOICE_PATH           = "VOICE_PATH";
    public final static String VOICE_DOWNLOAD_URL   = "VOICE_DOWNLOAD_URL";

    public final static boolean defStoreNumbers     = true;
    public final static String defVoicePath         = "/sdcard/Dialer/Voices/mamacita_us";
    public final static String defVoiceStoragePath  = "/sdcard/Dialer/Voices/";
    public final static String defVoiceDelete       = "";
    public final static String defVoiceDownloadURL  = "https://dt031g.programvaruteknik.nu/dialpad/sounds/";

    private static String getFilename(String fn) {
        return fn.substring(fn.lastIndexOf("/"));
    }

    private static boolean isEmptyValue(String val) {
        return ( (val==null) | (val.equals("")) | (val.equals("Default value")) | (val.equals("novalue"))   );
    }
    private static String getCustomSetting(Context context, String key, String defaultValue) {
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        String res = sh.getString(key, defaultValue);
        if (isEmptyValue(res))
            res = defaultValue;
        return res;
    }

    /**
     * recursively deletes this folder
     * @param f File / folder to be deleted.
     */
    private static void deleteFile(File f) {
        String[] flist = f.list();
        for (int i = 0; i < flist.length; i++) {
            System.out.println(" " + f.getAbsolutePath());
            File temp = new File(f.getAbsolutePath() + "/" + flist[i]);
            if (temp.isDirectory()) {
                deleteFile(temp);
                temp.delete();
            } else {
                temp.delete();
            }
        }
        f.delete();
    }


    private static boolean getCustomSetting(Context context, String key, boolean defaultValue) {
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        boolean res = sh.getBoolean(key, defaultValue);
        return res;
    }

    public static boolean getStoreNumbers(Context context) {
        return getCustomSetting(context,STORE_NUMBERS,defStoreNumbers);
    }

    public static String getVoicePath(Context context) {
        return getCustomSetting(context,VOICE_PATH,defVoicePath);
    }

    public static String getVoiceDownloadURL(Context context) {
        return getCustomSetting(context,VOICE_DOWNLOAD_URL,defVoiceDownloadURL);
    }

    public static String getVoiceStoragePath(Context context) {
        return getCustomSetting(context,VOICE_STORAGE_PATH,defVoiceStoragePath);
    }

    public static String getVoiceDelete(Context context) {
        return getCustomSetting(context,VOICE_DELETE,defVoiceDelete);
    }

    private static void setVoicePath(Context context, String value) {
        Log.d("SettingsAct.setVoicePa",String.format("Setting Voice path to : [%s]",defVoicePath));
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString(VOICE_PATH,value);
        editor.apply();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
        SwitchPreferenceCompat swStoreNumbers;
        ListPreference lpVoicePath;
        EditTextPreference etpVoiceDownload;
        EditTextPreference etpVoiceStoragePath;
        ListPreference lpVoiceDelete;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            Log.d("SettingsAct.onCreate","onCreatePreferences begin");
            swStoreNumbers = findPreference(STORE_NUMBERS);
            lpVoicePath = findPreference(VOICE_PATH);
            etpVoiceDownload = findPreference(VOICE_DOWNLOAD_URL);
            etpVoiceStoragePath = findPreference(VOICE_STORAGE_PATH);
            lpVoiceDelete = findPreference(VOICE_DELETE);
            Log.d("SettingsAct.onCreate","onCreatePreferences end");


                    //=======================================================================================
            setListPreferenceData(lpVoicePath,getActivity());
            // Set On Change Callback
            //lpVoicePath.setSummary(getVoicePath(getContext()));
            lpVoicePath.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String valueNew = newValue.toString();
                    String lpEntryName = valueNew.substring(valueNew.lastIndexOf("/"));
                    Log.d("SettingsFrag.onPrefC",String.format("Key : [%s]  New value: [%s]  EntryName: [%s]",preference.getKey(),valueNew,lpEntryName));
                    return true;
                }
            });
            // Set on click Callback
            lpVoicePath.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    setListPreferenceData(lpVoicePath, getActivity());
                    return true;
                }
            });

            //=======================================================================================
            // Set things related to the VOICE_DOWNLOAD_URL which is a EditTextPreference
            Log.d("SettingsAct.onCreate",VOICE_DOWNLOAD_URL);
            if (SettingsActivity.isEmptyValue(etpVoiceDownload.getText())) {
                Log.d("SettingsAct.onCreate","getText:"+etpVoiceDownload.getText());
                etpVoiceDownload.setText(SettingsActivity.getVoiceDownloadURL(getContext()));
            }
            etpVoiceDownload.setSummary(SettingsActivity.getVoiceDownloadURL(getContext()));
            etpVoiceDownload.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    etpVoiceDownload.setSummary(newValue.toString());
                    return true;
                }
            });

            //=======================================================================================
            // Set things related to the VOICE_STORAGE_PATH which is a EditTextPreference
            Log.d("SettingsAct.onCreate",VOICE_STORAGE_PATH);
            etpVoiceStoragePath = findPreference(VOICE_STORAGE_PATH);
            if (SettingsActivity.isEmptyValue(etpVoiceStoragePath.getText())) {
                etpVoiceStoragePath.setText(SettingsActivity.getVoiceStoragePath(getContext()));
            }
            etpVoiceStoragePath.setSummary(SettingsActivity.getVoiceStoragePath(getContext()));
            etpVoiceStoragePath.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    etpVoiceStoragePath.setSummary(newValue.toString());
                    return true;
                }
            });

            //=======================================================================================
            setListPreferenceData(lpVoiceDelete,getActivity());
            // Set On Change Callback
            String str = getVoiceDelete(getContext());
//            lpVoiceDelete.setSummary("Last deleted:"+str.substring(str.lastIndexOf("/")));
            lpVoiceDelete.setSummaryProvider(new Preference.SummaryProvider() {
                @Override
                public CharSequence provideSummary(Preference preference) {
                    String str = getVoiceDelete(getContext());
                    if (isEmptyValue(str))
                        return "Nothing was ever deleted.";
                    else
                        return "Last deleted: "+str.substring(str.lastIndexOf("/"));
                }
            });
            lpVoiceDelete.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String valueNew = newValue.toString();
                    String lpEntryName = valueNew.substring(valueNew.lastIndexOf("/"));
                    Log.d("SettingsAct.onPrefC",String.format("Key : [%s]  New value: [%s]  EntryName: [%s]",preference.getKey(),valueNew,lpEntryName));
                    String filename = newValue.toString();
                    File file = new File(filename);
                    if (file.exists()) {
                        deleteFile(file);
                    }

                    Log.d("SettingsAct.onPrefC",String.format("newvalue : [%s]  chosen: [%s]",valueNew,getVoicePath(getContext())));
                    // If the deleted value is the deleted value
                    if (valueNew.equals(getVoicePath(getContext()))) {
                        Log.d("SettingsAct.onPrefC",String.format("Setting Voice path to : [%s]",defVoicePath));
                        lpVoicePath.setValue(defVoicePath);
//                        setVoicePath(getContext(),defVoicePath);
//                        setListPreferenceData(lpVoicePath,getActivity());
                        SoundPlayer.getInstance(getContext()).initSound();
                        //lpVoicePath.setSummary(getFilename(getVoicePath(getContext()))+"+");
                    }

                    return true;
                }
            });
            // Set on click Callback
            lpVoiceDelete.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    setListPreferenceData(lpVoiceDelete, getActivity());
                    return true;
                }
            });
        }

        protected ListPreference setListPreferenceData(ListPreference lp, Activity mActivity) {

            // ArrayList storing entries
            ArrayList<String> alEntries = new ArrayList<>();
            // Array list storing entry values
            ArrayList<String> alEntryValues = new ArrayList<>();

            String path = getVoiceStoragePath(getContext());
            Log.d("SettingsAct.setList","Path: "+path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("SettingsAct.setList","Dir: "+directory);
            Log.d("SettingsAct.setList","Files count: "+files.length);
            for (int i = 0; i < files.length; i++)
            {
                Log.d("SettingsAct.setList","File: "+i+": "+files[i].getName());
                alEntries.add(files[i].getName());
                alEntryValues.add(files[i].getAbsolutePath());
            }
            Collections.sort(alEntries);
            Collections.sort(alEntryValues);

            Log.d("SettingsAct.setList", "alEntries" + alEntries);
            Log.d("SettingsAct.setList", "alEntryValues" + alEntryValues);

            if(lp == null) {
                lp = new ListPreference(mActivity);
            }
            // Initializing string array
            String[] entries     = new String[alEntries.size()];
            for (int i = 0; i<alEntries.size();i++)
                entries[i] = alEntries.get(i);
            String[] entryValues = new String[alEntryValues.size()];
            for (int i = 0; i<alEntryValues.size();i++)
                entryValues[i] = alEntryValues.get(i);

            Log.d("SettingsAct.setList", "Entries: " + entries);
            Log.d("SettingsAct.setList", "EntryValues: " + entryValues);

            lp.setEntries(entries);
            lp.setEntryValues(entryValues);
            lp.setDefaultValue("No voice stored.");
            //lp.setTitle("Select voice");
            lp.setDialogTitle("Select voice");

            return lp;
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Preference pref = findPreference(key);
            Toast.makeText(getContext(),"SettingsActivity.onSharedPreferenceChanged(): "+key + " SP: "+sharedPreferences.toString(),Toast.LENGTH_LONG).show();
            Log.d("SettingsAct.onSPC", "Key  (outside): " + key + " SP: "+sharedPreferences.toString());
        }
    }

    public void loadSettings() {

    }

    public void saveSettingss() {

    }
}