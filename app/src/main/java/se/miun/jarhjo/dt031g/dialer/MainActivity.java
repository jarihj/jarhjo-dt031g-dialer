package se.miun.jarhjo.dt031g.dialer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnDialClick(View view) {
        Intent intent = new Intent(this, DialActivity.class);
        startActivity(intent);
    }

    public void btnCallListClick(View view) {
        Intent intent = new Intent(this, CallListActivity.class);
        startActivity(intent);
    }

    public void btnSettingsClick(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void btnMapClick(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void btnDownloadVoicesClick(View view) {
        String strUrl = SettingsActivity.getVoiceDownloadURL(this);
        String strStorageLocation  = SettingsActivity.voiceDownloadSavePath;
        Intent intent = new Intent(this, DownloadActivity.class);
        intent.putExtra("url",strUrl);
        intent.putExtra("storageLocation",strStorageLocation);
        startActivity(intent);
    }

    public void btnAboutClick(View view) {
        String strTitle = getResources().getString(R.string.aboutTitle);
        String strMessage = getResources().getString(R.string.aboutMessage);
        new AlertDialog.Builder(this).setTitle(strTitle).setMessage(strMessage).setPositiveButton(getResources().getString(R.string.lblOK), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        ).show();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation==ORIENTATION_LANDSCAPE) {setContentView(R.layout.activity_main_landscape);}
        else {setContentView(R.layout.activity_main);}
    }
}
