package se.miun.jarhjo.dt031g.dialer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CallListActivity extends AppCompatActivity implements CallListReader {

    private static Set<String> calledNumbers = new HashSet<>();
    private static CallList callList;

    private TextView tvCallList;


    public static void saveNumber_old(Context context, String number) {
        Log.d("CallListActivity.saveNu","before");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.d("CallListActivity.saveNu","before 2");
        if (number.length()>0) {
            Log.d("CallListActivity.saveNu","inside before");
            calledNumbers.add(number);
            editor.putStringSet(SettingsActivity.CALL_LIST_KEY,calledNumbers);
            editor.apply();
            Log.d("CallListActivity.saveNu","inside after");
        }
        Log.d("CallListActivity.saveNu","after");
    }

    public static void saveNumber(Context context, String number, String time, String latitude, String longitude ) {
        if (SettingsActivity.getStoreNumbers(context)) {
            Log.d("DialActivity.saveNumber", "begin");
            // Store in server
            PhoneDatabase database = new PhoneDatabase(context);

            database.execute("add",number, time, latitude, longitude);
            Log.d("DialActivity.saveNumber", "end");
        }
    }


    public static void loadData_old(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> ss = sharedPreferences.getStringSet(SettingsActivity.CALL_LIST_KEY,null);
        if (ss!=null) {
            Log.d("CallListActivity.loadDa", sharedPreferences.getStringSet(SettingsActivity.CALL_LIST_KEY, null).toString());
            calledNumbers.addAll(sharedPreferences.getStringSet(SettingsActivity.CALL_LIST_KEY, null));
        } else {
            Log.d("CallListActivity.loadDa", "NO DATA: "+SettingsActivity.CALL_LIST_KEY);
        }

    }

    public void loadData(Context context) {
        Log.d("DialActivity.loadData", "begin");
        // Store in server
        callList = new CallList();
        PhoneDatabase database = new PhoneDatabase(context);
        database.callList = callList;
        database.callListReader=this;
        database.execute("getAll");
//        database.onPostExecute();
        Log.d("DialActivity.loadData", "end");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_list);

        tvCallList = (TextView) findViewById(R.id.tvCallList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData(this);
        tvCallList.setText("Loading...");
//        if (!calledNumbers.isEmpty()) {
//            Iterator<String> i = calledNumbers.iterator();
//            while (i.hasNext()) {
//                tvCallList.append(i.next() + "\n");
//            }
//        } else {
//            tvCallList.setText("No stored numbers.");
//        }

    }

    public void readCalls(List<Call> calls) {
        tvCallList.setText("");
        if (calls.size()>0) {
            String str="";
            for (int i=calls.size()-1;i>=0;i--) {
                str += String.format("%d : (%s,%s,%s,%s)\n", (calls.size()-i), calls.get(i).number, calls.get(i).time, calls.get(i).latitude, calls.get(i).longitude);

//                tvCallList.append(str+"\n");
            }
            tvCallList.setText(str);
        } else {
            tvCallList.setText("No stored numbers.");
        }


    }
}
