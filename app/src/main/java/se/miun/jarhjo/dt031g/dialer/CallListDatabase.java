package se.miun.jarhjo.dt031g.dialer;

import android.content.Context;
import android.util.Log;

import java.util.List;

import androidx.room.*;

public class CallListDatabase {
    private static CallListDatabase callListDatabase = null;
    private Context context = null;
    private AppDatabase db;

    private CallListDatabase(Context context) {
        this.context = context;
        db = Room.databaseBuilder(context,AppDatabase.class, "CallListDatabase").build();
    }

    public static CallListDatabase getInstance(Context context) {
        if (callListDatabase==null) {
            callListDatabase = new CallListDatabase(context);
        }
        return callListDatabase;
    }

    public static void destroyInstance() {
        callListDatabase = null;
    }


    public void addNumber(String number, String time, String latitude, String longitude) {
        int id = (int) System.currentTimeMillis();
        Call call = new Call(id, number,time,latitude,longitude);
        Log.d("CallListDatabase.addN",String.format("Data entry: (%d,%s,%s,%s,%s)",id,number,time,latitude,longitude));
        db.callDao().addNumber(call);
    }

    public List<Call> getAll() {
        Log.d("CallListDatabase.getA",String.format("begin"));
        List<Call> calls =  db.callDao().getAll();
        if (calls!=null) {
            int n = calls.size();
            Log.d("CallListDatabase.getA", String.format("Number of records: %d", n));
            for (int i=0; i<calls.size();i++) {
                Log.d("CallListDatabase.getA", String.format("Data %d : (%s,%s,%s,%s)",i,calls.get(i).number,calls.get(i).time,calls.get(i).latitude,calls.get(i).longitude));
            }
        }
        else {
            Log.d("CallListDatabase.getA", String.format("Number of records: NULL"));
        }
        Log.d("CallListDatabase.getA",String.format("end"));
        return calls;

    }
}

//https://developer.android.com/reference/android/arch/persistence/room/Entity
@Entity
class Call {
    @PrimaryKey
    public int id;
    public String number;
    public String time;
    public String latitude;
    public String longitude;

    public Call(int id,String number, String time, String latitude, String longitude) {
        this.id = id;
        this.number =number;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}


@Dao
interface CallDao {
    @Query("SELECT * FROM call")
    List<Call> getAll();

    @Query("SELECT * FROM call WHERE id IN (:callIds)")
    List<Call> loadAllByIds(int[] callIds);

    @Insert
    void insertAll(Call... calls);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addNumber(Call call);

    @Delete
    void delete(Call call);

    @Query("delete from call")
    void deleteAll();
}

// https://developer.android.com/training/data-storage/room/index.html
@Database(entities = {Call.class}, version = 1)
abstract class AppDatabase extends RoomDatabase {
    public abstract CallDao callDao();
}
