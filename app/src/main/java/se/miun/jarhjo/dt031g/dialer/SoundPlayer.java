package se.miun.jarhjo.dt031g.dialer;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

public class SoundPlayer {
    private static SoundPlayer soundPlayer;

    private Context context;
    private SoundPool soundPool;
    private int[] sounds;
    private boolean loadExternalSound = true;

    public static SoundPlayer getInstance(Context context) {
        if (soundPlayer==null) {
            soundPlayer = new SoundPlayer(context);
        }
        return soundPlayer;
    }

    public SoundPlayer(Context context) {
        sounds = new int[12];
        setContext(context);
        //initSound();

    }

    public void initSound() {
        initSound(loadExternalSound);
    }

    public void initSound(boolean useExternalSound) {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(12)
                    .setAudioAttributes(audioAttributes)
                    .build();

        } else {
            soundPool = new SoundPool(12, AudioManager.STREAM_MUSIC,0);
        }
        if (useExternalSound) loadSoundsMP3();
        else loadSoundsRes();
    }

    private void loadSoundsRes() {
        if (soundPool!=null) {
            sounds[0] = soundPool.load(getContext(), R.raw.zero, 1);
            sounds[1] = soundPool.load(getContext(), R.raw.one, 1);
            sounds[2] = soundPool.load(getContext(), R.raw.two, 1);
            sounds[3] = soundPool.load(getContext(), R.raw.three, 1);
            sounds[4] = soundPool.load(getContext(), R.raw.four, 1);
            sounds[5] = soundPool.load(getContext(), R.raw.five, 1);
            sounds[6] = soundPool.load(getContext(), R.raw.six, 1);
            sounds[7] = soundPool.load(getContext(), R.raw.seven, 1);
            sounds[8] = soundPool.load(getContext(), R.raw.eight, 1);
            sounds[9] = soundPool.load(getContext(), R.raw.nine, 1);
            sounds[10] = soundPool.load(getContext(), R.raw.star, 1);
            sounds[11] = soundPool.load(getContext(), R.raw.pound, 1);
        }
    }

    private static String addTrailingSlash(String str) {
        if (!str.endsWith("/")) str+="/";
        return str;
    }

    public void loadSoundsMP3() {
        String mp3Path = SettingsActivity.getVoicePath(context);
        Log.d("SoundPlayer","Path: "+SettingsActivity.getVoicePath(context));

        // Only load sounds if the soundPool is not null and permission to storage is given.
        if(soundPool != null) {
            // Load sounds
            sounds[0] = soundPool.load(addTrailingSlash(mp3Path) + "zero.mp3", 1);
            Log.d("SoundPlayer","Loading "+addTrailingSlash(mp3Path) + "zero.mp3");
            sounds[1] = soundPool.load(addTrailingSlash(mp3Path) + "one.mp3", 1);
            sounds[2] = soundPool.load(addTrailingSlash(mp3Path) + "two.mp3", 1);
            sounds[3] = soundPool.load(addTrailingSlash(mp3Path) + "three.mp3", 1);
            sounds[4] = soundPool.load(addTrailingSlash(mp3Path) + "four.mp3", 1);
            sounds[5] = soundPool.load(addTrailingSlash(mp3Path) + "five.mp3", 1);
            sounds[6] = soundPool.load(addTrailingSlash(mp3Path) + "six.mp3", 1);
            sounds[7] = soundPool.load(addTrailingSlash(mp3Path) + "seven.mp3", 1);
            sounds[8] = soundPool.load(addTrailingSlash(mp3Path) + "eight.mp3", 1);
            sounds[9] = soundPool.load(addTrailingSlash(mp3Path) + "nine.mp3", 1);
            sounds[10] = soundPool.load(addTrailingSlash(mp3Path) + "star.mp3", 1);
            sounds[11] = soundPool.load(addTrailingSlash(mp3Path) + "pound.mp3", 1);
        } else {
            // DTMF sounds.
        }
    }



    public void playSound(DialpadButton dialpadButton) {
        Log.d("SoundPlayer","playSound Begin: "+dialpadButton.getTitle());
        switch (dialpadButton.getTitle()) {
            case "*":
                soundPool.play(sounds[10], 1, 1, 0, 0, 1);
                break;
            case "#":
                soundPool.play(sounds[11], 1, 1, 0, 0, 1);
                break;
            default:
                int index = Integer.parseInt(dialpadButton.getTitle());
                soundPool.play(sounds[index], 1, 1, 0, 0, 1);
                Log.d("SoundPlayer","playSound played: "+dialpadButton.getTitle());
        } // switch
        Log.d("SoundPlayer","playSound End: "+dialpadButton.getTitle());

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Clean up the sound pool and resets the singleton object
     */
    public void destroy() {
        soundPool.release();
        soundPool = null;
        soundPlayer = null;
        sounds = null;
    }

}
