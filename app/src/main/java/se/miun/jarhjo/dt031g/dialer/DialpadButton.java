package se.miun.jarhjo.dt031g.dialer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * DialpadButton
 * A class representing a button for dialing
 * @author Bárður Bjartsson
 * @since 02.04.2020
 */
public class DialpadButton extends RelativeLayout {
    private String title;
    private String message;
    private TextView titleView;
    private TextView messageView;
    private SoundPlayer soundPlayer;
    private Dialpad dialpad;

    public Dialpad getDialpad() {
        return dialpad;
    }

    public void setDialpad(Dialpad dialpad) {
        this.dialpad = dialpad;
        Log.d("DialpadButton.setDialpa","Title: "+this.getTitle());
    }



    /**
     * Default Constructor
     * @param context - The context of the view
     */
    public DialpadButton(Context context) {
        super(context);
        init(context, null);
    }

    /**
     * Default Constructor
     * @param context - The context of the view
     * @param attrs - An attribute set
     */
    public DialpadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Default Constructor
     * @param context - The context of the view
     * @param attrs - An attribute set
     * @param defStyleAttr - Attribute of style resource
     */
    public DialpadButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initilizes the view
     * @param context - The context of the view
     * @param attrs - an attribute set
     */
    @SuppressLint("ClickableViewAccessibility")
    private void init(Context context, AttributeSet attrs) {
        // STYLE THE LAYOUT
        this.setBackgroundColor(Color.LTGRAY);

        // SET INFLATED LAYOUT
        inflate(context, R.layout.dialpad_button, this);
        titleView = findViewById(R.id.DialpadTitle);
        messageView = findViewById(R.id.DialpadMessage);

        // SET TITLE AND MESSAGE
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.dialpad_button, 0, 0);

        setTitle(a.getString(R.styleable.dialpad_button_title));
        setMessage(a.getString(R.styleable.dialpad_button_message));
        a.recycle();

        titleView.setText(title);
        messageView.setText(message);

        // Add on touch listener
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    scaleAnimation(1f, 0.9f, 70);
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    scaleAnimation(0.9f, 1f, 70);
                    soundPlayer.playSound((DialpadButton) v);
                    if (dialpad!=null) {
                        dialpad.onDialpadButtonClick((DialpadButton) v);
                    }
                    return true;
                }

                return false;
            }
        });

        if (!this.isInEditMode()) {
            soundPlayer = SoundPlayer.getInstance(getContext());
        }
    }

    /**
     * Sets the title of the view
     * @param title - a string representing a title in 1 digit
     */
    public void setTitle(String title) {
        if (title.length() >= 1)
        {
            this.title = title.substring(0, 1);
        }
        if (title.length() == 0)
        {
            this.title = "";
        }
    }

    public String getTitle() {
        return this.title;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * Sets the message of the view
     * @param message - a string representing a message in 1 to 4 digits
     */
    public void setMessage(String message) {
        if (message.length() > 4)
        {
            this.message = message.substring(0, 4);
        }
        if (message.length() <= 4)
        {
            this.message = message;
        }
    }

    /**
     * A scale animation that will scale the view to the desired size
     * @param startScale - start size of view
     * @param endScale - end size of view
     * @param animationTime - animation time as an integer in [mm] format
     */
    private void scaleAnimation(float startScale, float endScale, int animationTime) {
        final Animation scaleAnimation = new ScaleAnimation(startScale, endScale, startScale, endScale,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(animationTime);
        scaleAnimation.setFillAfter(true);
        this.startAnimation(scaleAnimation);
    }


}
