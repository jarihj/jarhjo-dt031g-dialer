package se.miun.jarhjo.dt031g.dialer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.File;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class DialActivity extends AppCompatActivity {
    // Get location as a string
    private String number = "";
    private String latitude = "??";
    private String longitude = "??";
    private boolean makeCalls = true;
    private Dialpad dialpad;
    private DialpadView dialpadView;
    private MenuItem btnGotoSettings;

    @Override
    protected void onResume() {
        super.onResume();
        loadVoices();
    }

    private void loadVoices() {
        if (checkReadPermission()) {
            Log.d("DialActivity.loadVoices", "Read external storage granted.");
            SoundPlayer.getInstance(this).initSound();
        } else SoundPlayer.getInstance(this).initSound(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dial);
        // Dynamic loading of XML Dialpad - WORKING
        //setContentView(R.layout.dialpad);
        // Dynamic loading of DialPad - WORKING
        //Dialpad dialpad = new Dialpad(this);
        //setContentView(dialpad);

        requestReadPermission();
        requestLocationPermission();

        getReadAccess();
        getSDCardPath();

        dialpad = (Dialpad) findViewById(R.id.dialpad);
        dialpadView = (DialpadView) findViewById(R.id.dialpadView);
        dialpad.setDialpadView(dialpadView);
        dialpadView.setDialActivity(this);


    }

    private void getReadAccess() {
        if (checkReadPermission()) {
            Log.d("DialActivity.onCreate", "Read external storage now granted.");
        } else {
            Log.d("DialActivity.onCreate", "Read external storage denied.");
        }
    }

    private void getSDCardPath() {
        File dd = Environment.getDataDirectory();
        File es = Environment.getExternalStorageDirectory();
        File rd = Environment.getRootDirectory();
        String sd = Environment.getExternalStorageDirectory().getAbsolutePath();

        Log.d("DialActivity.onCreate", "Data Directory:             " + dd.toString());
        Log.d("DialActivity.onCreate", "External Storage Directory: " + es.toString());
        Log.d("DialActivity.onCreate", "Root Directory:             " + rd.toString());
        Log.d("DialActivity.onCreate", "SD Directory:               " + sd.toString());
        File sdc = new File("/sdcard");
        if (sdc.exists()) {
            Log.d("DialActivity.onCreate", "SD Directory:               " + sdc.toString() + " exists.");
            if (sdc.isDirectory())
                Log.d("DialActivity.onCreate", "SD Directory:               " + sdc.toString() + " is directory.");
        } else {
            Log.d("DialActivity.onCreate", "SD Directory:               " + sdc.toString() + "exists NOT.");
        }
    }

    public boolean checkReadPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkCallPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
    }

    public boolean checkLocationPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public void requestReadPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PermissionsConstants.READ_PERMISSION_REQUEST_CODE);
    }

    public void requestCallPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PermissionsConstants.CALL_PERMISSION_REQUEST_CODE);
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermissionsConstants.LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dial_menu, menu);
        btnGotoSettings = menu.findItem(R.id.app_bar_goto_settings);

        btnGotoSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
                intent.putExtra(PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.class.getName());
                intent.putExtra(PreferenceActivity.EXTRA_NO_HEADERS, true);
                startActivity(intent);
                return false;
            }
        });
        return true;
    }

    public void call(String number) {
        this.number = number;
        saveNumber(number);
        if (makeCalls) {
            doCall(number);
        } else {
            forwardCall(number);
        }
    }

    private void saveNumber(String number) {
        if (SettingsActivity.getStoreNumbers(this)) {
            Log.d("DialActivity.saveN_old", "begin");
            readLocation();
            String time = Calendar.getInstance().getTime().toString();
            CallListActivity.saveNumber(this, number,time,latitude,longitude);
            Log.d("DialActivity.saveN_old", "end");
        }
    }


    private void forwardCall(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    private void doCall(String number) {
        requestCallPermission();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SoundPlayer.getInstance(this.getBaseContext()).destroy();
    }

    public boolean isMakeCalls() {
        return makeCalls;
    }

    public void setMakeCalls(boolean makeCalls) {
        this.makeCalls = makeCalls;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionsConstants.READ_PERMISSION_REQUEST_CODE:
                Log.d("DialActivity.OnRequestP", "Read Permissions: [[" + grantResults + "]] " + permissions);
                SoundPlayer.getInstance(this).initSound();
                break;
            case PermissionsConstants.WRITE_PERMISSION_REQUEST_CODE:
                Log.d("DialActivity.OnRequestP", "Write Permissions: [[" + grantResults + "]] " + permissions);
                break;
            case PermissionsConstants.CALL_PERMISSION_REQUEST_CODE:
                Log.d("DialActivity.OnRequestP", "Call Permissions: [[" + grantResults + "]] " + permissions);
                if (checkCallPermission()) {
                    Log.d("DialActivity.OnRequestP", "Call access granted.");
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                } else {
                    Log.d("DialActivity.OnRequestP", "Call access denied.");
                }

                break;
            case PermissionsConstants.LOCATION_PERMISSION_REQUEST_CODE:
                Log.d("DialActivity.OnRequestP", "Location Permissions: [[" + grantResults.toString() + "]] " + permissions);

                readLocation();
                break;
            default:
                Log.d("DialActivity.OnRequestP", "SHOULD NOT OCCUR!!!! Permissions: [[" + grantResults + "]] " + permissions);
        }
    }

    private void readLocation() {

        Log.d("DialActivity.readLoc", "readLocaion(): begin");
        if (checkLocationPermission()) {
            Log.d("DialActivity.readLoc", "readLocaion(): true");
            LocationManager lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());
                Log.d("DialActivity.readLoc", String.format("readLocaion(): lat:%s  lon:%s",latitude,longitude));
            }
        }
        else {
            Log.d("DialActivity.readLoc", "readLocation(): false");
            latitude = "NaN";
            longitude = "NaN";
        }
    }
}
