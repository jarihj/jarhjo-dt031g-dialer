package se.miun.jarhjo.dt031g.dialer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;



public class Dialpad extends ConstraintLayout {
    private DialpadView dialpadView;

    /**
     * Default Constructor
     * @param context - The context of the view
     */
    public Dialpad(Context context) {
        super(context);
        init(context, null);
    }

    /**
     * Default Constructor
     * @param context - The context of the view
     * @param attrs - An attribute set
     */
    public Dialpad(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Default Constructor
     * @param context - The context of the view
     * @param attrs - An attribute set
     * @param defStyleAttr - Attribute of style resource
     */
    public Dialpad(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initilizes the view
     * @param context - The context of the view
     * @param attrs - an attribute set
     */
    @SuppressLint("ClickableViewAccessibility")
    private void init(Context context, AttributeSet attrs) {
        // SET INFLATED LAYOUT
        inflate(context, R.layout.dialpad, this);
        DialpadButton dialpadButton;
        ((DialpadButton) findViewById(R.id.dialpad_button_0)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_1)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_2)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_3)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_4)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_5)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_6)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_7)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_8)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_9)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_star)).setDialpad(this);
        ((DialpadButton) findViewById(R.id.dialpad_button_pound)).setDialpad(this);
    }

    public DialpadView getDialpadView() {
        return dialpadView;
    }

    public void setDialpadView(DialpadView dialpadView) {
        this.dialpadView = dialpadView;
    }

    public void onDialpadButtonClick(DialpadButton dialpadButton) {
        Log.d("Dialpad.onButtonClick","The text of the button is: "+ dialpadButton.getTitle());
        if (dialpadView!=null) {
            dialpadView.addChar(dialpadButton.getTitle().charAt(0));
        }
    }
}

