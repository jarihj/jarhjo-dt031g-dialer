package se.miun.jarhjo.dt031g.dialer;

import java.util.List;

public interface CallListReader {
    public void readCalls(List<Call> calls);
}

