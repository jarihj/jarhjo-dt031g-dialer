package se.miun.jarhjo.dt031g.dialer;

public class PermissionsConstants {
    public final static int READ_PERMISSION_REQUEST_CODE = 200;
    public final static int WRITE_PERMISSION_REQUEST_CODE = 201;
    public final static int CALL_PERMISSION_REQUEST_CODE = 202;
    public final static int LOCATION_PERMISSION_REQUEST_CODE = 203;

    public final static int LOCATION_CALL_PERMISSION_REQUEST_CODE = 210;

}
