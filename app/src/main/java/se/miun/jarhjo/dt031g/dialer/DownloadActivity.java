package se.miun.jarhjo.dt031g.dialer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;


public class DownloadActivity extends AppCompatActivity {
    private class DownloadWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Toast.makeText(getApplicationContext(),"URL: "+url,Toast.LENGTH_LONG).show();
            Log.d("DownloadWVC.shouldOverr","URL: "+url);
            return false;
//            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            startActivity(intent);
//            return true;
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Long> {
        String saveFile; // full path
        String fileName; // only filename

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("DownloadTask.onPre","starting");
            progressBar.setMax(100);
            progressBar.setProgress(0);
            getTvProgressPercent.setText("0 %");
            showProgressView();
        }

        protected Long doInBackground(String... urls) {
            String url = urls[0];
            long writtenBytes = 0;
            int bytesRead = -1;
            int bytesSize = 0;

            try {
                Log.d("DownloadTask.doInBack","url: "+url);
                URL u = new URL(url);
                URLConnection urlConnection = null;
                urlConnection = u.openConnection();

                InputStream inputStream = urlConnection.getInputStream();

                Log.d("DownloadTask.doInBack","index of / : "+url.lastIndexOf('/'));
                Log.d("DownloadTask.doInBack","index of . : "+url.lastIndexOf('.'));
                fileName = url.substring(url.lastIndexOf('/')+1, url.length());
                Log.d("DownloadTask.doInBack","filename: "+fileName);
                saveFile = getStorageLocation()+fileName;
                File storageLocation = new File(getStorageLocation());
                if (!storageLocation.exists()) {
                    storageLocation.mkdirs();
                    Log.d("DownloadTask.doInBack","Dirs created: "+storageLocation.toString());
                }
                Log.d("DownloadTask.doInBack","saveFile: "+saveFile);
                FileOutputStream outputStream = new FileOutputStream(saveFile);
                byte[] buffer = new byte[1024];
                bytesSize = urlConnection.getContentLength();
                Log.d("DownloadTask.doInBack","before setprogress");

                Integer [] pub = {0,1};

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                    writtenBytes+=bytesRead;
                    pub[0] = (int) writtenBytes;
                    pub[1] = bytesSize;
                    publishProgress(pub);
                    //Thread.sleep(1);
                }
                outputStream.close();
                inputStream.close();
                Log.d("DownloadTask.doInBack",String.format("Download finished. %d bytes written.",writtenBytes));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return writtenBytes;
        }

        protected void onProgressUpdate(Integer... progress) {
            Log.d("DownloadTask.onProgre",String.format("Progress %d of %d.",progress[0],progress[1]));
            setProgressMax(progress[1]);
            setProgressPosition(progress[0]);
        }

        protected void onPostExecute(Long result) {
            Log.d("DownloadTask.onPost",String.format("Download finished. %d bytes written.",result));
            ZIP.decompress(saveFile,SettingsActivity.getVoiceStoragePath(getApplication()));
            new File(saveFile).delete();
            hideProgressView();
        }

    }

    private String storageLocation;
    private String url;
    private WebView wvWebView;
    private DownloadWebViewClient webViewClient;
    private ConstraintLayout clDownload;
    private ProgressBar progressBar;
    private TextView tvProgressText;
    private TextView getTvProgressPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        wvWebView = findViewById(R.id.wvWebView);
        webViewClient = new DownloadWebViewClient();
        wvWebView.setWebViewClient(webViewClient);
        wvWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Toast.makeText(getApplicationContext(),"URL: "+url,Toast.LENGTH_LONG).show();
                Log.d("DownloadActivity.onDown","URL: "+url);
                tvProgressText.setText(String.format("Downloading %s ...",url.substring(url.lastIndexOf('/')+1, url.length())));
                new DownloadTask().execute(url);
            }
        });
        clDownload = (ConstraintLayout) findViewById(R.id.clDownload);
        tvProgressText = (TextView) findViewById(R.id.tvProgressText);
        getTvProgressPercent = (TextView) findViewById(R.id.tvProgressPercent);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    private void foreignSaveMethod(String url, String userAgent, String contentDisposition, String mimetype) {
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url));
        request.setMimeType(mimetype);
        String cookies = CookieManager.getInstance().getCookie(url);
        request.addRequestHeader("cookie", cookies);
        request.addRequestHeader("User-Agent", userAgent);
        request.setDescription("Downloading file...");
        request.setTitle(URLUtil.guessFileName(url, contentDisposition,
                mimetype));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(
                        url, contentDisposition, mimetype));
        DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(request);
        Toast.makeText(getApplicationContext(), "Downloading File",
                Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWriteAccess();

        setUrl(getIntent().getExtras().getString("url"));
        setStorageLocation(getIntent().getExtras().getString("storageLocation"));
        wvWebView.loadUrl(getUrl());
        Log.d("DialActivity.onCreate","Write external storage granted.");

    }

    private void getWriteAccess() {
        //requestPermission();
        if (checkWritePermission()) {
            Log.d("DialActivity.onCreate","Write external storage granted.");
        } else {
            Log.d("DialActivity.onCreate","Asking for permission to external storage.");
            if (requestWritePermission()) {Log.d("DialActivity.onCreate","Write external storage now granted."); }
            else {Log.d("DialActivity.onCreate","Write external storage denied.");}
        }
    }

    public boolean checkWritePermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED);
    }

    public boolean requestWritePermission() {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},PermissionsConstants.WRITE_PERMISSION_REQUEST_CODE);
        return checkWritePermission();
    }


    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionsConstants.READ_PERMISSION_REQUEST_CODE :
                Log.d("DownloadAct.OnRequestP","Read Permissions: [["+grantResults+"]] "+permissions);
                break;
            case PermissionsConstants.WRITE_PERMISSION_REQUEST_CODE :
                Log.d("DownloadAct.OnRequestP","Write Permissions: [["+grantResults+"]] "+permissions);
                break;
            case PermissionsConstants.CALL_PERMISSION_REQUEST_CODE :
                Log.d("DownloadAct.OnRequestP","Call Permissions: [["+grantResults+"]] "+permissions);
                break;
            default:
                Log.d("DownloadAct.OnRequestP","SHOULD NOT OCCUR!!!! Permissions: [["+grantResults+"]] "+permissions);
        }
    }

    private void showProgressView() {
        clDownload.setVisibility(View.VISIBLE);
        clDownload.bringToFront();
    }

    private void hideProgressView() {
        clDownload.setVisibility(View.INVISIBLE);
    }

    public void btnClick(View view) {

        if (clDownload.getVisibility()==View.VISIBLE)  {
            hideProgressView();
        } else {
            showProgressView();
        }
    }

    private void setProgressPosition(int value) {
        progressBar.setProgress((int)  value);
        getTvProgressPercent.setText(String.format("%.0f %%",100.0*progressBar.getProgress()/progressBar.getMax()));
    }

    private void setProgressMax(int value) {
        progressBar.setMax((int) value);
    }

    private void setProgressText(String value) {
        tvProgressText.setText(value);
    }


}



